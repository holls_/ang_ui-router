const app = angular.module('materialApp', ['ngMaterial', 'ui.router'])
    .config(config).component('hello', {
        templateUrl: "./src/templates/helloGalaxy.template.html",
        controller: function () {
            this.greetings = "Ola boutarde!!!"

            this.toogleGreetings = () => {
                this.greetings = this.greetings === "Ola boutarde!!!" ? 'what up' : "Ola boutarde!!!"
                console.log(this.greetings)
            }

            // this.
        }
    }).run(($uiRouter) => {
        //Executa quando a aplicação tiver iniciado, ou "bootstraped"
        var StateTree = window['ui-router-visualizer'].StateTree;
        var el = StateTree.create($uiRouter, null, { height: 100, width: 300 });
        el.className = 'statevis';
        window.onload = () => {
            document.querySelector("#routerVisualizer").appendChild(el)
        }
        console.log('Application Running')
    
    })

function config($stateProvider) {
    const home = {
        name: 'home',
        url: '/',
        template: "<h3 style='color: blue;'>Home Page</h3>"
    }
    const about = {
        name: 'about',
        url: '/about',
        template: "<h3 style='color: green;'>About Page</h3>"
    }

    const helloGalaxy = {
        name: 'galaxy',
        url: "/galaxy",
        component: 'hello'
    }

    $stateProvider.state(home)
    $stateProvider.state(about)
    $stateProvider.state(helloGalaxy)
}
        